# 练口琴(play the harmonica)的最终用户许可协议

本最终用户许可协议（“EULA”）是您与贸碟技术之间的法律协议。我们的EULA是由 [EULA模板](https://www.eulatemplate.com/) 创建的，用于“练口琴”软件。

本EULA协议适用于您直接从贸碟技术或间接通过贸碟技术授权经销商或分销商（“经销商”）购买和使用我们的“练口琴”软件（“软件”）。我们的隐私策略是由隐私策略生成器创建的。

在完成安装过程并使用“练口琴”软件之前，请仔细阅读此EULA协议。它提供使用口琴软件的许可证，并包含保修信息和免责声明。

如果您注册免费试用“练口琴”软件，此EULA协议也适用于该试用。单击“接受”或安装和/或使用“练口琴”软件，即表示您确认接受该软件，并同意受本EULA协议条款的约束。如果您代表公司或其他法律实体签订本EULA协议，您表示您有权约束该实体及其附属公司遵守这些条款和条件。如果您没有此权限或不同意本EULA协议的条款和条件，请不要安装或使用本软件，并且您也不得接受本EULA协议。

本EULA协议仅适用于贸碟技术提供的软件，而不管此处是否提及或描述其他软件。本条款也适用于贸碟技术的任何软件更新、补充、基于互联网的服务和支持服务，除非交付时附带其他条款。如果是这样，这些条款适用。

# 许可证授予

贸碟技术特此授予您根据本EULA协议条款在您的设备上使用“练口琴”软件的个人、不可转让、非排他性许可。

您可以在您的控制下加载口琴软件（例如PC、笔记本电脑、手机或平板电脑）。您有责任确保您的设备符合吹奏口琴软件的最低要求。

您不得：

- 编辑、更改、修改、改编、翻译或以其他方式更改本软件的全部或任何部分，也不允许本软件的全部或任何部分与任何其他软件相结合或合并在一起，也不允许对本软件进行反编译、反汇编或逆向工程，或试图进行任何此类事情

- 为任何商业目的复制、复制、分发、转售或以其他方式使用本软件

- 允许任何第三方代表或为任何第三方的利益使用本软件

- 以任何违反当地、国家或国际适用法律的方式使用本软件

- 将本软件用于贸碟技术认为违反本EULA协议的任何目的

# 知识产权和所有权
 
贸碟技术应始终保留您最初下载的软件以及您随后下载的所有软件的所有权。本软件（以及本软件的版权和其他任何性质的知识产权，包括对其所做的任何修改）均为贸碟技术的财产。

贸碟技术保留向第三方授予软件使用许可的权利。

# 终止

本EULA协议自您首次使用本软件之日起生效，并将持续到终止。您可以随时书面通知贸碟技术终止本协议。

如果您未能遵守本EULA协议的任何条款，本协议也将立即终止。一旦终止，本EULA协议授予的许可将立即终止，您同意停止对软件的所有访问和使用。本EULA协议终止后，根据其性质继续有效的条款将继续有效。

# 适用法律

本EULA协议，以及因本EULA协议而产生或与之相关的任何争议，均应受中国法律管辖并按其进行解释。

2020.12