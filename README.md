# 练口琴

## 介绍

练口琴（play the harmonica）软件为用户提供演奏乐曲的口琴实时提示，帮助用户学习口琴。

## 特性

- 支持 10孔全音阶口琴、半音阶口琴（10孔、12孔、14孔、16孔）、复音口琴（24孔、28孔）
- 支持2种提示方式：瀑布条模式、吹吸模式
- 支持针对修改乐曲演奏速度、改变乐曲演奏调式、改变口琴类型以及口琴调式
- 支持设置不同的音符提示方式：123、doremmi、CDE
- 支持用户修改吹吸提示色彩
- 允许用户针对具有多种演奏方式的音符进行演奏策略设置
- 提供约250首自带乐曲
- 用户在获得权限后，可以自己添加abc格式的乐曲。

## 下载地址

谷歌应用市场下载地址：

[谷歌1](https://play.google.com/store/apps/details?id=com.moudge.playtheharmonica)

腾讯应用市场下载地址：

[PC端](https://android.myapp.com/myapp/detail.htm?apkName=com.moudge.playtheharmonica)

[移动端](https://a.app.qq.com/o/simple.jsp?pkgname=com.moudge.playtheharmonica)



## 帮助

相关帮助文档请参考

[中文帮助](https://docs.qq.com/doc/DRVlUbE9UbXBFUmF2?pub=1&dver=2.1.0)

[English help](https://docs.qq.com/doc/DRVBEZFJVWVV3TWh6?pub=1&dver=2.1.0)

## abc乐曲的编辑

abc格式以及相关信息参考网站 [abcnotation](http://abcnotation.com/)

编辑工具参考 [EasyABC](https://sourceforge.net/projects/easyabc/files/EasyABC/)

## 联系方式

用户可以发送电子邮件到contact@moudge.com与我联系。

或者 在gitee中提交问题。

或者 加入QQ群：618362259  练口琴1。

## 获取添加乐曲专辑权限的方法

添加乐曲专辑权限可以通过软件内观看激励广告的方式兑换获得。

或者通过淘宝购买。

