# Play the harmonica

## Introduce

The software(Play the harmonica) provides users with real-time tips when playing music with harmonica, to help users learn harmonica.

## Features

- Support 10 hole diatonic harmonica, chromatic harmonica (10 hole, 12 hole, 14 hole, 16 hole),tremolo harmonica (24 hole, 28 hole)
- Supports two display modes: Waterfall mode and Drawing-blowing mode
- Support for the modification of music performance speed, change of music playing mode, change of harmonica type and harmonica key
- Support to set different note tips mode：123、doremmi、CDE
- Support users to modify the color of blowing and drawing
- It allows users to set playing strategies for notes with multiple playing methods
- There are 50 different free musics and 150 pieces of exchangeable musics
- Users can add songs in ABC format by themselves after obtaining the permission.

## Download 

Google Play download url：

[google](https://play.google.com/store/apps/details?id=com.moudge.playtheharmonica)

Tencent Myapp download url：

[PC](https://android.myapp.com/myapp/detail.htm?apkName=com.moudge.playtheharmonica)

[Mobile](https://a.app.qq.com/o/simple.jsp?pkgname=com.moudge.playtheharmonica)

## Help

Please refer to

[中文帮助](https://docs.qq.com/doc/DRVlUbE9UbXBFUmF2?pub=1&dver=2.1.0)

[English help](https://docs.qq.com/doc/DRVBEZFJVWVV3TWh6?pub=1&dver=2.1.0)

## abc music editor

ABC format and related information reference website [abcnotation](http://abcnotation.com/)

Editing tool reference [EasyABC](https://sourceforge.net/projects/easyabc/files/EasyABC/)

## Contact

Users can send email to contact@moudge.com Contact me.

Or submit a question in gitee.

or join QQ group：618362259  玩口琴1。

## How to get permission to add music and album

The permission to add music and album can be exchange by watching rewarded advertisement in the software.

Or through Taobao.

