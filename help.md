# 中文帮助
本软件的中文帮助文档,请点击下面的链接查看:
[帮助](https://docs.qq.com/doc/DRVlUbE9UbXBFUmF2?pub=1&dver=2.1.0)

# help(English)
Click the link below for the help of this software:
[help](https://docs.qq.com/doc/DRVBEZFJVWVV3TWh6?pub=1&dver=2.1.0)
